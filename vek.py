#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import sys

#HG-Nummern verschiedener Elemente
hg_nr = {
       'H':1, 'Li':1, 'Na':1,  'K':1, 'Rb':1, 'Cs':1,
      'Be':2, 'Mg':2, 'Ca':2, 'Sr':2, 'Ba':2,
       'B':3, 'Al':3, 'Ga':3, 'In':3, 'Tl':3,
       'C':4, 'Si':4, 'Ge':4, 'Sn':4, 'Pb':4,
       'N':5,  'P':5, 'As':5, 'Sb':5, 'Bi':5,
       'O':6,  'S':6, 'Se':6, 'Te':6,
       'F':7, 'Cl':7, 'Br':7,  'I':7
     }

def parse_formula(formula):
    #Parser für Summenformel
    #Regular expression für Element, Anzahl
    pattern = re.compile(r'([A-Z][a-z]*)(\d*)')

    # Finde, was dazu passt
    matches = pattern.findall(formula)

    # Dictionary um die Werte zu speichern
    elements = {}

    # Durch Treffer iterieren und Dict füllen
    for match in matches:
        element, count = match
        count = int(count) if count else 1
        try:
            elements[element] = {'count':count, 'hg_nr':hg_nr[element]}
        except KeyError:
            print(f'{element} nicht in der Liste der unterstützen Elemente!')
            sys.exit(1)
    return elements

def vek(formula):
    #Berechnung VEK
    cation = list(parsed_formula.keys())[0]
    anion = list(parsed_formula.keys())[1]
    m   = formula[cation].get('count')
    e_M = formula[cation].get('hg_nr')
    x   = formula[anion].get('count')
    e_X = formula[anion].get('hg_nr')
    vek = (m*e_M + x*e_X)/x
    return vek
    
def b_xx(vek):
    #Berechnung b_xx
    b_xx = 8 - vek
    return b_xx

def b_ges(formula):
    #Berechnung b_ges
    cation = list(parsed_formula.keys())[0]
    anion = list(parsed_formula.keys())[1]
    m   = formula[cation].get('count')
    e_M = formula[cation].get('hg_nr')
    x   = formula[anion].get('count')
    e_X = formula[anion].get('hg_nr')
    vek = (m*e_M + x*e_X)/x
    b_xx = 8 - vek
    b_ges = (b_xx * x)/2
    return b_ges

# Example usage:
formula = input('Eingabe der Summenformel in der Form AxBy, z.B. Na4Pb4: ')

if formula == '':
    formula = "Na4Pb4"
    print("Na4Pb4")

parsed_formula = parse_formula(formula)

if len(parsed_formula) != 2:    
    print('Es müssen genau zwei Elemente angegeben werden, z.B. Na4Pb4')
    sys.exit(1)


vek = vek(parsed_formula)

print(f'\nVEK = {vek:.2f}\n') 

if vek == 8:
    print('VEK = 8 => einfach ionisch')

elif vek < 8:
    b_xx = b_xx(vek)
    b_ges = b_ges(parsed_formula)
    print('VEK ist < 8 => polyanionisch')
    print(f'b(XX) = {b_xx:.2f}')
    print(f'b_ges = {b_ges:.2f}\n')
    print('Achtung! b_ges ist nicht immer sinnvoll berechenbar.')

elif vek > 8:
    print('VEK > 8 => polykationisch')